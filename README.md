# Connnect spark to R with sparklyr

## Getting started

See [scripts/install_load_spark](scripts/install_load_spark.R), which is a script to load and install Spark locally, and connect it to R via the R package `sparklyr`. More information can be found at https://spark.rstudio.com/guides/connections/


## Examples

### Example 1: diamonds data

See [scripts/sparklyr_diamonds_analysis.R](scripts/sparklyr_diamonds_analysis.R) for loading the diamonds dataset from the R package `ggplot2` into Spark, and apply some basic statistics as well as machine learning.

### Example 2: Dutch road sensors data

See [scripts/sparklyr_road_sensor_processing.R](scripts/sparklyr_road_sensor_processing.R) for reading Dutch road sensor data and load it into Spark.

Examples of visualization can be found in [sparklyr_road_sensor_visualization.R](sparklyr_road_sensor_visualization.R).


## Home assignment

Connect R to different Spark installations:

- Local Spark installation
- [Google Codelabs](colab.to/r)
- Any other Spark installation that is available at your organization

Explore the Dutch road sensor data. Create the following visualizations:

- Make a time series plot of all road sensors.
- Make a map of all road sensors at specific times, e.g. rush hours.

Answer these questions:

- Which road is the busiest on average?
- Which roads have large differences between the two directions (L and R) during rush hours?

Finally, explore your own data with R-Spark.
